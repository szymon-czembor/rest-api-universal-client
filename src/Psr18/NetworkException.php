<?php

declare(strict_types=1);

namespace Rauc\Psr18;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Client\NetworkExceptionInterface;
use Exception;

class NetworkException extends Exception implements NetworkExceptionInterface
{
    private RequestInterface $request;

    public function __construct(string $message, RequestInterface $request)
    {
        parent::__construct($message);

        $this->request = $request;
    }

    public function getRequest(): RequestInterface
    {
        return $this->request;
    }
}
