<?php

declare(strict_types=1);

namespace Rauc\Psr18;

use Psr\Http\Client\{ClientInterface, ClientExceptionInterface};
use Psr\Http\Message\{RequestInterface, ResponseInterface};
use Rauc\Psr17\FactoryInterface;
use Rauc\Psr7\RequestValidatorInterface;
use Rauc\CurlWrapperInterface;

class CurlClient implements ClientInterface
{
    private const TIMEOUT_IN_SECONDS = 10;

    private CurlWrapperInterface $curlWrapper;
    private FactoryInterface $psr17Factory;
    private RequestInterface $request;
    private RequestValidatorInterface $psr7RequestValidator;

    public function __construct(
        CurlWrapperInterface $curlWrapper,
        FactoryInterface $psr17Factory,
        RequestValidatorInterface $psr7RequestValidator
    ) {
        $this->curlWrapper = $curlWrapper;
        $this->psr17Factory = $psr17Factory;
        $this->psr7RequestValidator = $psr7RequestValidator;
    }

    /**
     * @param RequestInterface $request
     * 
     * @return ResponseInterface
     * 
     * @throws ClientExceptionInterface
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        $this->psr7RequestValidator->validate($request);
        $this->request = $request;

        $this->initCurl();
        $this->setCurlOptions($request);
        $this->executeCurlSession();

        return $this->createResponse();
    }

    private function initCurl(): void
    {
        $this->curlWrapper->init()
            ->setTimeout(self::TIMEOUT_IN_SECONDS);
    }

    private function setCurlOptions(RequestInterface $request): void
    {
        $url = $request->getUri()
            ->__toString();

        $this->curlWrapper->setHttpMethod($request->getMethod())
            ->setUrl($url);

        $this->handlePostFields($request);
        $this->handleHeaders($request);
    }

    private function executeCurlSession(): void
    {
        $this->curlWrapper->execute()
            ->close();
    }

    private function handlePostFields(RequestInterface $request): void
    {
        $method = $request->getMethod();

        if (!$this->canAddBody($method)) {
            return;
        }

        $body = $request->getBody()
            ->__toString();

        if (empty($body)) {
            return;
        }

        $this->curlWrapper->setPostFields($body);
    }

    private function handleHeaders(RequestInterface $request): void
    {
        $headers = $request->getHeaders();

        if (empty($headers)) {
            return;
        }

        $headerNames = array_keys($headers);
        $headerLines = [];

        foreach ($headerNames as $header) {
            $headerLines[$header] = $request->getHeaderLine($header);
        }

        $this->curlWrapper->setRequestHeaders($headerLines);
    }

    /**
     * @return ResponseInterface
     * 
     * @throws NetworkException
     */
    private function createResponse(): ResponseInterface
    {
        if ($this->curlWrapper->getErrorCode() > 0) {
            $errorMessage = $this->curlWrapper->getErrorMessage();

            throw new NetworkException($errorMessage, $this->request);
        }

        $statusCode = $this->curlWrapper->getStatusCode();
        $curlResponse = $this->curlWrapper->getResponse();
        $responseHeaders = $this->curlWrapper->getResponseHeaders();

        $response = $this->psr17Factory->createResponse($statusCode);

        if (is_string($curlResponse) && strlen($curlResponse) > 0) {
            $stream = $this->psr17Factory->createStream($curlResponse);
            $response = $response->withBody($stream);
        }

        foreach ($responseHeaders as $header => $value) {
            $response = $response->withHeader($header, $value);
        }

        return $response;
    }

    private function canAddBody(string $method): bool
    {
        return in_array($method, ['POST', 'PUT', 'PATCH'], true);
    }
}
