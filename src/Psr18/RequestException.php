<?php

declare(strict_types=1);

namespace Rauc\Psr18;

use Exception;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Client\RequestExceptionInterface;

class RequestException extends Exception implements RequestExceptionInterface
{
    private RequestInterface $request;

    public function __construct(string $message, RequestInterface $request)
    {
        parent::__construct($message);
        $this->request = $request;
    }

    public function getRequest(): RequestInterface
    {
        return $this->request;
    }
}
