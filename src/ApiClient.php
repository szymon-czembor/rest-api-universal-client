<?php

declare(strict_types=1);

namespace Rauc;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\{RequestInterface, ResponseInterface};
use Rauc\Authentication\AuthenticationExceptionInterface;
use Rauc\Authentication\AuthenticationMethodInterface;
use Rauc\Psr17\FactoryInterface;

class ApiClient
{
    private ClientInterface $httpClient;
    private FactoryInterface $psr17Factory;
    private string $baseUrl;
    private ?AuthenticationMethodInterface $authMethod;

    public function __construct(ClientInterface $httpClient, FactoryInterface $psr17Factory)
    {
        $this->httpClient = $httpClient;
        $this->psr17Factory = $psr17Factory;
        $this->baseUrl = '';
        $this->authMethod = null;
    }

    public function setBaseUrl(string $url): ApiClient
    {
        $this->baseUrl = $url;

        return $this;
    }

    public function setAuthMethod(AuthenticationMethodInterface $authMethod): ApiClient
    {
        $this->authMethod = $authMethod;

        return $this;
    }

    /**
     * @param string $endpoint
     * 
     * @return ResponseInterface
     * 
     * @throws ApiClientException
     */
    public function get(string $endpoint): ResponseInterface
    {
        return $this->request('GET', $endpoint);
    }

    /**
     * @param string $endpoint
     * @param array $data
     * 
     * @return ResponseInterface
     * 
     * @throws ApiClientException
     */
    public function post(string $endpoint, array $data): ResponseInterface
    {
        return $this->request('POST', $endpoint, $data);
    }

    /**
     * @param string $endpoint
     * @param array $data
     * 
     * @return ResponseInterface
     * 
     * @throws ApiClientException
     */
    public function put(string $endpoint, array $data): ResponseInterface
    {
        return $this->request('PUT', $endpoint, $data);
    }

    /**
     * @param string $endpoint
     * @param array $data
     * 
     * @return ResponseInterface
     * 
     * @throws ApiClientException
     */
    public function patch(string $endpoint, array $data): ResponseInterface
    {
        return $this->request('PATCH', $endpoint, $data);
    }
    
    /**
     * @param string $endpoint
     * 
     * @return ResponseInterface
     * 
     * @throws ApiClientException
     */
    public function head(string $endpoint): ResponseInterface
    {
        return $this->request('HEAD', $endpoint);
    }

    /**
     * @param string $endpoint
     * 
     * @return ResponseInterface
     * 
     * @throws ApiClientException
     */
    public function options(string $endpoint): ResponseInterface
    {
        return $this->request('OPTIONS', $endpoint);
    }

    /**
     * @param string $endpoint
     * 
     * @return ResponseInterface
     * 
     * @throws ApiClientException
     */
    public function delete(string $endpoint): ResponseInterface
    {
        return $this->request('DELETE', $endpoint);
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param array $data
     * 
     * @return ResponseInterface
     * 
     * @throws ApiClientException
     */
    private function request(string $method, string $endpoint, array $data = []): ResponseInterface
    {
        $uri = $this->baseUrl . $endpoint;
        $request = $this->psr17Factory->createRequest($method, $uri);

        if ($this->authMethod !== null) {
            $request = $this->authenticate($request);
        }
        
        if (in_array($method, ['POST', 'PUT', 'PATCH'])) {
            $request = $this->addBody($request, $data);
            $request = $request->withHeader('Content-Type', 'application/json');
        }

        return $this->sendRequest($request);
    }

    private function addBody(RequestInterface $request, array $data): RequestInterface
    {
        $stream = $this->psr17Factory->createStream(json_encode($data));
        
        return $request->withBody($stream);
    }

    /**
     * @param RequestInterface $request
     * 
     * @return RequestInterface
     * 
     * @throws ApiClientException
     */
    private function authenticate(RequestInterface $request): RequestInterface
    {
        try {
            return $this->authMethod->authenticate($request);
        } catch (AuthenticationExceptionInterface $e) {
            throw new ApiClientException($e->getMessage());
        }
    }

    /**
     * @param RequestInterface $request
     * 
     * @return ResponseInterface
     * 
     * @throws ApiClientException
     */
    private function sendRequest(RequestInterface $request): ResponseInterface
    {
        try {
            return $this->httpClient->sendRequest($request);
        } catch (ClientExceptionInterface $e) {
            throw new ApiClientException($e->getMessage());
        }
    }
}
