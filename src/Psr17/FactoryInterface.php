<?php

namespace Rauc\Psr17;

use Psr\Http\Message\{
    RequestFactoryInterface,
    ResponseFactoryInterface,
    UriFactoryInterface,
    StreamFactoryInterface
};

interface FactoryInterface extends RequestFactoryInterface, ResponseFactoryInterface, UriFactoryInterface, StreamFactoryInterface
{
}
