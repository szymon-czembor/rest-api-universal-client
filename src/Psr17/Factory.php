<?php

declare(strict_types=1);

namespace Rauc\Psr17;

use InvalidArgumentException;
use Psr\Http\Message\{
    RequestInterface,
    ResponseInterface,
    UriInterface,
    StreamInterface
};
use Nyholm\Psr7\Factory\Psr17Factory;

class Factory implements FactoryInterface
{
    private Psr17Factory $factory;

    public function __construct()
    {
        $this->factory = new Psr17Factory();
    }

    /**
     * @param string $method
     * @param UriInterface|string $uri
     *
     * @return RequestInterface
     */
    public function createRequest(string $method, $uri): RequestInterface
    {
        return $this->factory->createRequest($method, $uri);
    }

    /**
     * @param int $code
     * @param string $reasonPhrase
     *
     * @return ResponseInterface
     */
    public function createResponse(int $code = 200, string $reasonPhrase = ''): ResponseInterface
    {
        return $this->factory->createResponse($code, $reasonPhrase);
    }

    /**
     * @param string $uri
     * 
     * @return UriInterface
     * 
     * @throws InvalidArgumentException
     */
    public function createUri(string $uri = ''): UriInterface
    {
        return $this->factory->createUri($uri);
    }

    /**
     * @param string $content
     *
     * @return StreamInterface
     */
    public function createStream(string $content = ''): StreamInterface
    {
        return $this->factory->createStream($content);
    }

    /**
     * @param string $filename
     * @param string $mode
     *
     * @return StreamInterface
     */
    public function createStreamFromFile(string $filename, string $mode = 'r'): StreamInterface
    {
        return $this->factory->createStreamFromFile($filename, $mode);
    }

    /**
     * @param resource $resource
     *
     * @return StreamInterface
     */
    public function createStreamFromResource($resource): StreamInterface
    {
        return $this->factory->createStreamFromResource($resource);
    }
}
