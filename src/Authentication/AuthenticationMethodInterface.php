<?php

namespace Rauc\Authentication;

use Psr\Http\Message\RequestInterface;

interface AuthenticationMethodInterface
{
    /**
     * @param RequestInterface $request
     * 
     * @return RequestInterface
     * 
     * @throws AuthenticationExceptionInterface
     */
    public function authenticate(RequestInterface $request): RequestInterface;
}
