<?php

declare(strict_types=1);

namespace Rauc\Authentication;

use Psr\Http\Message\RequestInterface;

class BasicAuthentication implements AuthenticationMethodInterface
{
    private ?string $username = null;
    private ?string $password = null;

    public function setUsername(string $username): BasicAuthentication
    {
        $this->username = $username;

        return $this;
    }

    public function setPassword(string $password): BasicAuthentication
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param RequestInterface $request
     *
     * @return RequestInterface
     *
     * @throws AuthenticationException
     */
    public function authenticate(RequestInterface $request): RequestInterface
    {
        if ($this->username === null || $this->password === null) {
            throw new AuthenticationException('Missing credentials');
        }

        $credentials = base64_encode($this->username . ':' . $this->password);
        
        return $request->withHeader('Authorization', 'Basic ' . $credentials);
    }
}
