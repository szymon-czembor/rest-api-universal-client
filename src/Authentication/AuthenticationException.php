<?php

namespace Rauc\Authentication;

use Exception;

class AuthenticationException extends Exception implements AuthenticationExceptionInterface
{
}
