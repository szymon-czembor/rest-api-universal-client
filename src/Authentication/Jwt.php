<?php

declare(strict_types=1);

namespace Rauc\Authentication;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Rauc\Psr17\FactoryInterface;
use Psr\Http\Message\{RequestInterface, ResponseInterface};

class Jwt implements AuthenticationMethodInterface
{
    private ClientInterface $httpClient;
    private FactoryInterface $psr17Factory;
    private array $credentials;
    private ?string $url;
    private ?string $tokenPath;

    public function __construct(ClientInterface $httpClient, FactoryInterface $psr17Factory)
    {
        $this->httpClient = $httpClient;
        $this->psr17Factory = $psr17Factory;
        $this->credentials = [];
        $this->url = null;
        $this->tokenPath = null;
    }

    public function setCredentials(array $credentials): Jwt
    {
        $this->credentials = $credentials;

        return $this;
    }

    public function setAuthUrl(string $url): Jwt
    {
        $this->url = $url;

        return $this;
    }

    public function setTokenPath(string $tokenPath): Jwt
    {
        $this->tokenPath = $tokenPath;

        return $this;
    }

    /**
     * @param RequestInterface $request
     * 
     * @return RequestInterface
     * 
     * @throws AuthenticationException
     */
    public function authenticate(RequestInterface $request): RequestInterface
    {
        $this->validateEssentials();

        $response = $this->sendAuthRequest();

        $this->checkIfAuthenticationFailed($response->getStatusCode());

        $token = $this->fetchTokenFromResponse($response);
        
        return $request->withHeader('Authorization', 'Bearer ' . $token);
    }

    /**
     * @throws AuthenticationException
     */
    private function validateEssentials(): void
    {
        if (empty($this->credentials)) {
            throw new AuthenticationException('Missing credentials');
        }

        if ($this->url === null) {
            throw new AuthenticationException('Missing authentication URL');
        }

        if ($this->tokenPath === null) {
            throw new AuthenticationException('Missing token path');
        }
    }

    /**
     * @return ResponseInterface
     * 
     * @throws AuthenticationException
     */
    private function sendAuthRequest(): ResponseInterface
    {
        try {
            return $this->httpClient->sendRequest($this->createAuthRequest());
        } catch (ClientExceptionInterface $e) {
            throw new AuthenticationException('HTTP client error');
        }
    }

    private function createAuthRequest(): RequestInterface
    {
        $json = json_encode($this->credentials);
        $stream = $this->psr17Factory->createStream($json);
        $request = $this->psr17Factory->createRequest('POST', $this->url);
        
        return $request->withBody($stream)
            ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param int $statusCode
     *
     * @throws AuthenticationException
     */
    private function checkIfAuthenticationFailed(int $statusCode): void
    {
        if ($statusCode !== 200) {
            throw new AuthenticationException('Authentication failed');
        }
    }

    /**
     * @param ResponseInterface $response
     * 
     * @return string
     * 
     * @throws AuthenticationException
     */
    private function fetchTokenFromResponse(ResponseInterface $response): string
    {
        $body = (string) $response->getBody();
        $parsedBody = json_decode($body, true);
        
        return $this->fetchTokenFromParsedBody($parsedBody);
    }

    /**
     * @param array $parsedBody
     * 
     * @return string
     * 
     * @throws AuthenticationException
     */
    private function fetchTokenFromParsedBody(array $parsedBody): string
    {
        $tokenPath = explode('.', $this->tokenPath);
        $token = $parsedBody;

        foreach ($tokenPath as $item) {
            if (!isset($token[$item])) {
                throw new AuthenticationException('Token not found');
            }

            $token = $token[$item];
        }

        if (!is_string($token)) {
            throw new AuthenticationException('Token not found');
        }

        return $token;
    }
}
