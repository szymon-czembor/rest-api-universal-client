<?php

namespace Rauc\Authentication;

use Throwable;

interface AuthenticationExceptionInterface extends Throwable
{
}
