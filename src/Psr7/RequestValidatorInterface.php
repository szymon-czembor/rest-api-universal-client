<?php

namespace Rauc\Psr7;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Client\RequestExceptionInterface;

interface RequestValidatorInterface
{
    /**
     * @param RequestInterface $request
     * 
     * @throws RequestExceptionInterface
     */
    public function validate(RequestInterface $request): void;
}
