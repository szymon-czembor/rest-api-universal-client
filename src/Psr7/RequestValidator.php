<?php

declare(strict_types=1);

namespace Rauc\Psr7;

use Psr\Http\Message\RequestInterface;
use Rauc\Psr18\RequestException;

class RequestValidator implements RequestValidatorInterface
{
    /**
     * @param RequestInterface $request
     * 
     * @throws RequestException
     */
    public function validate(RequestInterface $request): void
    {
        $this->validateMethod($request);
        $this->validateProtocolVersion($request);
        $this->validateUri($request);
    }

    /**
     * @param RequestInterface $request
     * 
     * @throws RequestException
     */
    private function validateMethod(RequestInterface $request): void
    {
        $method = $request->getMethod();

        if ($request->getMethod() === '') {
            throw new RequestException('Empty method', $request);
        }

        if (!in_array($method, ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS', 'HEAD', 'CONNECT', 'TRACE'], true)) {
            throw new RequestException('Invalid method', $request);
        }
    }

    /**
     * @param RequestInterface $request
     * 
     * @throws RequestException
     */
    private function validateProtocolVersion(RequestInterface $request): void
    {
        if ($request->getProtocolVersion() === '') {
            throw new RequestException('Empty protocol version', $request);
        }
    }

    /**
     * @param RequestInterface $request
     *
     * @throws RequestException
     */
    private function validateUri(RequestInterface $request): void
    {
        $uri = $request->getUri();

        if ($uri->__toString() === '') {
            throw new RequestException('Empty URI', $request);
        }
    }
}
