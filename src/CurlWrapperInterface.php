<?php

namespace Rauc;

interface CurlWrapperInterface
{
    /**
     * Initialize a cURL session
     * 
     * @return CurlWrapperInterface
     */
    public function init(): CurlWrapperInterface;

    /**
     * Set a request method
     * Valid values: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS, TRACE, CONNECT
     * 
     * @param string $method
     * 
     * @return CurlWrapperInterface
     */
    public function setHttpMethod(string $method): CurlWrapperInterface;

    /**
     * Set URL to fetch
     * 
     * @param string $url
     * 
     * @return CurlWrapperInterface
     */
    public function setUrl(string $url): CurlWrapperInterface;

    /**
     * Set timeout in seconds
     * 
     * @param int $timeout
     * 
     * @return CurlWrapperInterface
     */
    public function setTimeout(int $timeout): CurlWrapperInterface;

    /**
     * Set data to post in a request
     * 
     * @param string $data
     * 
     * @return CurlWrapperInterface
     */
    public function setPostFields(string $data): CurlWrapperInterface;

    /**
     * Set a request header
     * 
     * @param string $header
     * @param string $value
     * 
     * @return CurlWrapperInterface
     */
    public function setRequestHeader(string $header, string $value): CurlWrapperInterface;

    /**
     * Set request headers in the format: ['Content-Type: application/json', 'Content-Length: 100']
     * 
     * @param array<string> $headers
     * 
     * @return CurlWrapperInterface
     */
    public function setRequestHeaders(array $headers): CurlWrapperInterface;

    /**
     * Perform a cURL session
     * 
     * @return CurlWrapperInterface
     */
    public function execute(): CurlWrapperInterface;

    /**
     * Close a cURL session
     */
    public function close(): void;

    /**
     * Return response
     * 
     * @return mixed
     */
    public function getResponse();

    /**
     * Return status code
     * 
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * Return array of response headers
     * The key is a header name and the value is a header value
     * 
     * @return array<string>
     */
    public function getResponseHeaders(): array;

    /**
     * Return a response header value
     * If there is no header, the method returns null
     *
     * @param string $name
     *
     * @return string|null
     */
    public function getResponseHeader(string $name): ?string;

    /**
     * Return the error code or 0 if there was no error
     * 
     * @return int
     */
    public function getErrorCode(): int;

    /**
     * Return the error message or empty string if there was no error
     * 
     * @return string
     */
    public function getErrorMessage(): string;
}
