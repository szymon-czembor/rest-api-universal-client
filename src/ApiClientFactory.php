<?php

namespace Rauc;

use Rauc\Psr17\Factory;
use Rauc\Psr18\CurlClient;
use Rauc\Psr7\RequestValidator;

class ApiClientFactory
{
    public static function create(): ApiClient
    {
        $curlWrapper = new CurlWrapper();
        $psr17Factory = new Factory();
        $requestValidator = new RequestValidator();
        $httpClient = new CurlClient($curlWrapper, $psr17Factory, $requestValidator);

        return new ApiClient($httpClient, $psr17Factory);
    }
}
