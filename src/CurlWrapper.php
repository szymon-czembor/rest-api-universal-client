<?php

declare(strict_types=1);

namespace Rauc;

class CurlWrapper implements CurlWrapperInterface
{
    /**
     * @var resource|null $curl
     */
    private $curl;
    private $response;
    private array $requestHeaders;
    private array $responseHeaders;
    private ?int $statusCode;
    private ?int $errorCode;
    private ?string $errorMessage;

    public function init(): CurlWrapperInterface
    {
        $this->reset();
        $this->curl = curl_init();

        return $this;
    }

    public function setHttpMethod(string $method): CurlWrapperInterface
    {
        if ($method === 'HEAD') {
            curl_setopt($this->curl, CURLOPT_NOBODY, true);
        } else {
            curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, $method);
        }

        return $this;
    }

    public function setUrl(string $url): CurlWrapperInterface
    {
        curl_setopt($this->curl, CURLOPT_URL, $url);

        return $this;
    }

    public function setTimeout(int $timeout): CurlWrapperInterface
    {
        curl_setopt($this->curl, CURLOPT_TIMEOUT, $timeout);

        return $this;
    }

    public function setPostFields(string $data): CurlWrapperInterface
    {
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);

        return $this;
    }

    public function setRequestHeader(string $header, string $value): CurlWrapperInterface
    {
        $this->requestHeaders[] = $header . ': ' . $value;

        return $this;
    }

    public function setRequestHeaders(array $headers): CurlWrapperInterface
    {
        $requestHeaders = [];

        foreach ($headers as $header => $value) {
            $requestHeaders[] = $header . ': ' . $value;
        }

        $this->requestHeaders = $requestHeaders;

        return $this;
    }

    public function execute(): CurlWrapperInterface
    {
        $this->fetchResponseHeaders();
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->requestHeaders);

        $this->response = curl_exec($this->curl);
        $this->statusCode = curl_getinfo($this->curl, CURLINFO_RESPONSE_CODE);
        $this->errorCode = curl_errno($this->curl);
        $this->errorMessage = curl_error($this->curl);

        return $this;
    }

    public function close(): void
    {
        curl_close($this->curl);
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getResponseHeaders(): array
    {
        return $this->responseHeaders;
    }

    public function getResponseHeader(string $name): ?string
    {
        return $this->responseHeaders[$name] ?? null;
    } 

    public function getErrorCode(): int
    {
        return $this->errorCode;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    private function fetchResponseHeaders(): void
    {
        curl_setopt($this->curl, CURLOPT_HEADERFUNCTION, function ($curl, $header) {
            $length = strlen($header);
            $header = explode(':', $header, 2);
        
            if (count($header) < 2) {
                return $length;
            }
        
            $this->responseHeaders[trim($header[0])] = trim($header[1]);
        
            return $length;
        });
    }

    private function reset(): void
    {
        $this->curl = null;
        $this->response = null;
        $this->requestHeaders = [];
        $this->responseHeaders = [];
        $this->statusCode = null;
        $this->errorCode = null;
        $this->errorMessage = null;
    }
}
