<?php

namespace Rauc\Authentication;

use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;

class BasicAuthenticationTest extends TestCase
{
    public function testSuccessfulAuthentication(): void
    {
        $basicAuthentication = new BasicAuthentication();

        $credentials = base64_encode('username:password');

        $requestWithoutCredentials = $this->createMock(RequestInterface::class);
        $requestWithCredentials = $this->createMock(RequestInterface::class);

        $requestWithoutCredentials->expects($this->once())
            ->method('withHeader')
            ->with('Authorization', 'Basic ' . $credentials)
            ->willReturn($requestWithCredentials);

        $result = $basicAuthentication->setUsername('username')
            ->setPassword('password')
            ->authenticate($requestWithoutCredentials);

        $this->assertSame($requestWithCredentials, $result);
    }

    public function testUsernameNotSet(): void
    {
        $basicAuthentication = new BasicAuthentication();

        $requestWithoutCredentials = $this->createMock(RequestInterface::class);

        $requestWithoutCredentials->expects($this->never())
            ->method('withHeader');

        $this->expectException(AuthenticationException::class);
        $this->expectExceptionMessage('Missing credentials');

        $basicAuthentication->setPassword('password')
            ->authenticate($requestWithoutCredentials);
    }

    public function testPasswordNotSet(): void
    {
        $basicAuthentication = new BasicAuthentication();

        $requestWithoutCredentials = $this->createMock(RequestInterface::class);

        $requestWithoutCredentials->expects($this->never())
            ->method('withHeader');

        $this->expectException(AuthenticationException::class);
        $this->expectExceptionMessage('Missing credentials');

        $basicAuthentication->setUsername('username')
            ->authenticate($requestWithoutCredentials);
    }
}
