<?php

namespace Rauc\Authentication;

use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\{RequestInterface, ResponseInterface, StreamInterface};
use Rauc\Psr17\Factory;
use Rauc\Psr17\FactoryInterface;
use Rauc\Psr18\NetworkException;

class JwtTest extends TestCase
{
    private FactoryInterface $realPsr17Factory;
    private $httpClient;
    private $psr17Factory;
    private Jwt $jwt;
    private $stream;

    public function setUp(): void
    {
        $this->realPsr17Factory = new Factory();
        $this->httpClient = $this->createMock(ClientInterface::class);
        $this->psr17Factory = $this->createMock(FactoryInterface::class);
        $this->jwt = new Jwt($this->httpClient, $this->psr17Factory);
        $this->stream = $this->createMock(StreamInterface::class);
    }

    public function testSuccessfulAuthentication(): void
    {
        $credentials = [
            'login' => 'username',
            'password' => 'pass',
        ];

        $json = json_encode($credentials);

        $this->psr17Factory->expects($this->once())
            ->method('createStream')
            ->with($json)
            ->willReturn($this->stream);

        $request = $this->createMock(RequestInterface::class);

        $this->psr17Factory->expects($this->once())
            ->method('createRequest')
            ->with('POST', 'https://example.com/login')
            ->willReturn($request);

        $request->expects($this->once())
            ->method('withHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn($request);

        $request->expects($this->once())
            ->method('withBody')
            ->with($this->identicalTo($this->stream))
            ->willReturn($request);

        $this->httpClient->method('sendRequest')
            ->with($this->identicalTo($request))
            ->willReturn($this->createResponseWithToken());

        $request = $this->createMock(RequestInterface::class);

        $request->expects($this->once())
            ->method('withHeader')
            ->with('Authorization', 'Bearer fake.jwt.token')
            ->willReturn($request);

        $result = $this->jwt->setCredentials($credentials)
            ->setAuthUrl('https://example.com/login')
            ->setTokenPath('token.path')
            ->authenticate($request);

        $this->assertSame($request, $result);
    }

    public function testMissingCredentials(): void
    {
        $request = $this->createMock(RequestInterface::class);

        $this->expectException(AuthenticationException::class);
        $this->expectExceptionMessage('Missing credentials');

        $this->jwt->setAuthUrl('https://example.com/login')
            ->setTokenPath('token.path')
            ->authenticate($request);
    }

    public function testMissingAuthUrl(): void
    {
        $request = $this->createMock(RequestInterface::class);

        $this->expectException(AuthenticationException::class);
        $this->expectExceptionMessage('Missing authentication URL');

        $this->jwt->setCredentials([
                'login' => 'username',
                'password' => 'pass',
            ])
            ->setTokenPath('token.path')
            ->authenticate($request);
    }

    public function testMissingTokenPath(): void
    {
        $request = $this->createMock(RequestInterface::class);

        $this->expectException(AuthenticationException::class);
        $this->expectExceptionMessage('Missing token path');

        $this->jwt->setCredentials([
                'login' => 'username',
                'password' => 'pass',
            ])
            ->setAuthUrl('https://example.com/login')
            ->authenticate($request);
    }

    public function testHttpClientError(): void
    {
        $this->expectException(AuthenticationException::class);
        $this->expectExceptionMessage('HTTP client error');

        $request = $this->createMock(RequestInterface::class);

        $this->psr17Factory->method('createRequest')
            ->willReturn($request);

        $request->method('withBody')
            ->willReturn($request);

        $request->method('withHeader')
            ->willReturn($request);

        $this->httpClient->method('sendRequest')
            ->willThrowException(new NetworkException('Error', $request));

        $this->jwt->setCredentials([
                'login' => 'username',
                'password' => 'pass',
            ])
            ->setAuthUrl('https://example.com/login')
            ->setTokenPath('token.path')
            ->authenticate($request);
    }

    public function testFailedAuthentication(): void
    {
        $credentials = [
            'login' => 'username',
            'password' => 'pass',
        ];

        $json = json_encode($credentials);

        $this->psr17Factory->expects($this->once())
            ->method('createStream')
            ->with($json)
            ->willReturn($this->stream);

        $request = $this->createMock(RequestInterface::class);

        $this->psr17Factory->expects($this->once())
            ->method('createRequest')
            ->with('POST', 'https://example.com/login')
            ->willReturn($request);

        $request->expects($this->once())
            ->method('withHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn($request);

        $request->expects($this->once())
            ->method('withBody')
            ->with($this->identicalTo($this->stream))
            ->willReturn($request);

        $this->httpClient->method('sendRequest')
            ->with($this->identicalTo($request))
            ->willReturn($this->createFailedAuthenticationResponse());

        $request = $this->createMock(RequestInterface::class);

        $this->expectException(AuthenticationException::class);
        $this->expectExceptionMessage('Authentication failed');

        $this->jwt->setCredentials($credentials)
            ->setAuthUrl('https://example.com/login')
            ->setTokenPath('token.path')
            ->authenticate($request);
    }

    public function testTokenNotFound(): void
    {
        $credentials = [
            'login' => 'username',
            'password' => 'pass',
        ];

        $json = json_encode($credentials);

        $this->psr17Factory->method('createStream')
            ->with($json)
            ->willReturn($this->stream);

        $request = $this->createMock(RequestInterface::class);

        $this->psr17Factory->method('createRequest')
            ->with('POST', 'https://example.com/login')
            ->willReturn($request);

        $request->method('withHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn($request);

        $request->method('withBody')
            ->with($this->identicalTo($this->stream))
            ->willReturn($request);

        $this->httpClient->method('sendRequest')
            ->with($this->identicalTo($request))
            ->willReturn($this->createResponseWithToken());

        $request = $this->createMock(RequestInterface::class);

        $this->expectException(AuthenticationException::class);
        $this->expectExceptionMessage('Token not found');

        $this->jwt->setCredentials($credentials)
            ->setAuthUrl('https://example.com/login')
            ->setTokenPath('token')
            ->authenticate($request);
    }

    public function testTooLongTokenPath(): void
    {
        $credentials = [
            'login' => 'username',
            'password' => 'pass',
        ];

        $json = json_encode($credentials);

        $this->psr17Factory->method('createStream')
            ->with($json)
            ->willReturn($this->stream);

        $request = $this->createMock(RequestInterface::class);

        $this->psr17Factory->method('createRequest')
            ->with('POST', 'https://example.com/login')
            ->willReturn($request);

        $request->method('withHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn($request);

        $request->method('withBody')
            ->with($this->identicalTo($this->stream))
            ->willReturn($request);

        $this->httpClient->method('sendRequest')
            ->with($this->identicalTo($request))
            ->willReturn($this->createResponseWithToken());

        $request = $this->createMock(RequestInterface::class);

        $this->expectException(AuthenticationException::class);
        $this->expectExceptionMessage('Token not found');

        $this->jwt->setCredentials($credentials)
            ->setAuthUrl('https://example.com/login')
            ->setTokenPath('path.to.token')
            ->authenticate($request);
    }

    private function createResponseWithToken(): ResponseInterface
    {
        $response = $this->realPsr17Factory->createResponse(200);
        $token = json_encode([
            'token' => [
                'path' => 'fake.jwt.token',
            ]
        ]);

        $stream = $this->realPsr17Factory->createStream($token);
        
        return $response->withBody($stream)
            ->withHeader('Content-Type', 'application/json');
    }

    private function createFailedAuthenticationResponse(): ResponseInterface
    {
        $response = $this->realPsr17Factory->createResponse(400);
        
        $token = json_encode([
            'error' => 'Invalid credentials',
        ]);

        $stream = $this->realPsr17Factory->createStream($token);
        
        return $response->withBody($stream)
            ->withHeader('Content-Type', 'application/json');
    }
}
