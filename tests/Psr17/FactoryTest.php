<?php

namespace Rauc\Psr17;

use PHPUnit\Framework\TestCase;
use Psr\Http\Message\{RequestInterface, ResponseInterface, StreamInterface, UriInterface};

class FactoryTest extends TestCase
{
    private Factory $factory;

    public function setUp(): void
    {
        $this->factory = new Factory();
    }

    public function testRequest(): void
    {
        $request = $this->factory->createRequest('GET', 'https://example.com');

        $this->assertInstanceOf(RequestInterface::class, $request);
    }

    public function testRequestWithUriInterface(): void
    {
        $request = $this->factory->createRequest('GET', $this->factory->createUri('https://example.com'));

        $this->assertInstanceOf(RequestInterface::class, $request);
    }

    public function testResponse(): void
    {
        $response = $this->factory->createResponse(200, 'OK');

        $this->assertInstanceOf(ResponseInterface::class, $response);
    }

    public function testResponseWithDefaultArguments(): void
    {
        $response = $this->factory->createResponse();

        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('', $response->getReasonPhrase());
    }

    public function testUri(): void
    {
        $uri = $this->factory->createUri('https://example.com');

        $this->assertInstanceOf(UriInterface::class, $uri);
    }

    public function testUriDefaultArgument(): void
    {
        $uri = $this->factory->createUri();

        $this->assertInstanceOf(UriInterface::class, $uri);
    }

    public function testStreamFromString(): void
    {
        $stream = $this->factory->createStream('content');

        $this->assertInstanceOf(StreamInterface::class, $stream);
    }
}
