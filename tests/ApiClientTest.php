<?php

namespace Rauc;

use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Rauc\Authentication\AuthenticationMethodInterface;
use Rauc\Psr17\FactoryInterface;
use Rauc\Authentication\AuthenticationException;
use Rauc\Psr18\NetworkException;

class ApiClientTest extends TestCase
{
    private $httpClient;
    private $psr17Factory;
    private ApiClient $apiClient;
    private $request;
    private $authenticatedRequest;
    private $response;
    private $authMethod;
    private $stream;
    private $requestWithBody;

    public function setUp(): void
    {
        $this->httpClient = $this->createMock(ClientInterface::class);
        $this->psr17Factory = $this->createMock(FactoryInterface::class);
        $this->apiClient = new ApiClient($this->httpClient, $this->psr17Factory);
        $this->request = $this->createMock(RequestInterface::class);
        $this->authenticatedRequest = $this->createMock(RequestInterface::class);
        $this->response = $this->createMock(ResponseInterface::class);
        $this->authMethod = $this->createMock(AuthenticationMethodInterface::class);
        $this->stream = $this->createMock(StreamInterface::class);
        $this->requestWithBody = $this->createMock(RequestInterface::class);
    }

    public function testGetRequestWithoutAuthentication(): void
    {
        $this->psr17Factory->expects($this->once())
            ->method('createRequest')
            ->with('GET', 'https://example.com/posts')
            ->willReturn($this->request);

        $this->authMethod->expects($this->never())
            ->method('authenticate');

        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->with($this->identicalTo($this->request))
            ->willReturn($this->response);

        $result = $this->apiClient->setBaseUrl('https://example.com')
            ->get('/posts');

        $this->assertSame($this->response, $result);
    }

    public function testGetMethodWithAuthentication(): void
    {
        $this->psr17Factory->expects($this->once())
            ->method('createRequest')
            ->with('GET', 'https://example.com/posts')
            ->willReturn($this->request);

        $this->authMethod->expects($this->once())
            ->method('authenticate')
            ->with($this->identicalTo($this->request))
            ->willReturn($this->authenticatedRequest);
            
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->with($this->identicalTo($this->authenticatedRequest))
            ->willReturn($this->response);

        $result = $this->apiClient->setBaseUrl('https://example.com')
            ->setAuthMethod($this->authMethod)
            ->get('/posts');

        $this->assertSame($this->response, $result);
    }

    public function testPostRequestWithoutAuthentication(): void
    {
        $post = [
            'title' => 'Post Title',
            'content' => 'Content',
        ];

        $this->psr17Factory->expects($this->once())
            ->method('createRequest')
            ->with('POST', 'https://example.com/posts')
            ->willReturn($this->request);

        $this->authMethod->expects($this->never())
            ->method('authenticate');

        $this->psr17Factory->expects($this->once())
            ->method('createStream')
            ->with(json_encode($post))
            ->willReturn($this->stream);

        $this->request->expects($this->once())
            ->method('withBody')
            ->with($this->stream)
            ->willReturn($this->requestWithBody);

        $this->requestWithBody->expects($this->once())
            ->method('withHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn($this->requestWithBody);
            
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->with($this->identicalTo($this->requestWithBody))
            ->willReturn($this->response);

        $result = $this->apiClient->setBaseUrl('https://example.com')
            ->post('/posts', $post);

        $this->assertSame($this->response, $result);
    }

    public function testPostRequestWithAuthentication(): void
    {
        $post = [
            'title' => 'Post Title',
            'content' => 'Content',
        ];

        $this->psr17Factory->expects($this->once())
            ->method('createRequest')
            ->with('POST', 'https://example.com/posts')
            ->willReturn($this->request);

        $this->authMethod->expects($this->once())
            ->method('authenticate')
            ->with($this->identicalTo($this->request))
            ->willReturn($this->authenticatedRequest);

        $this->psr17Factory->expects($this->once())
            ->method('createStream')
            ->with(json_encode($post))
            ->willReturn($this->stream);

        $this->authenticatedRequest->expects($this->once())
            ->method('withBody')
            ->with($this->stream)
            ->willReturn($this->requestWithBody);

        $this->requestWithBody->expects($this->once())
            ->method('withHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn($this->requestWithBody);
            
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->with($this->identicalTo($this->requestWithBody))
            ->willReturn($this->response);

        $result = $this->apiClient->setBaseUrl('https://example.com')
            ->setAuthMethod($this->authMethod)
            ->post('/posts', $post);

        $this->assertSame($this->response, $result);
    }

    public function testFailedAuthentication(): void
    {
        $this->authMethod->method('authenticate')
            ->willThrowException(new AuthenticationException());

        $this->expectException(ApiClientException::class);

        $this->apiClient->setBaseUrl('https://example.com')
            ->setAuthMethod($this->authMethod)
            ->get('/posts');
    }

    public function testFailedRequest(): void
    {
        $this->httpClient->method('sendRequest')
            ->willThrowException(new NetworkException('Error', $this->request));

        $this->expectException(ApiClientException::class);

        $this->apiClient->setBaseUrl('https://example.com')
            ->setAuthMethod($this->authMethod)
            ->get('/posts');
    }
}
