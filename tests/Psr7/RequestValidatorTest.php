<?php

namespace Rauc\Psr7;

use PHPUnit\Framework\TestCase;
use Psr\Http\Message\{RequestInterface, UriInterface};
use Psr\Http\Client\RequestExceptionInterface;

class RequestValidatorTest extends TestCase
{
    private RequestValidator $requestValidator;

    public function setUp(): void
    {
        $this->requestValidator = new RequestValidator();
    }

    public function testEmptyMethod(): void
    {
        $request = $this->createMock(RequestInterface::class);

        $request->method('getMethod')
            ->willReturn('');

        $this->expectException(RequestExceptionInterface::class);
        $this->expectExceptionMessage('Empty method');

        $this->requestValidator->validate($request);
    }

    public function testInvalidMethod(): void
    {
        $request = $this->createMock(RequestInterface::class);

        $request->method('getMethod')
            ->willReturn('get');

        $this->expectException(RequestExceptionInterface::class);
        $this->expectExceptionMessage('Invalid method');

        $this->requestValidator->validate($request);
    }

    public function testEmptyProtocolVersion(): void
    {
        $request = $this->createMock(RequestInterface::class);

        $request->method('getMethod')
            ->willReturn('GET');

        $request->method('getProtocolVersion')
            ->willReturn('');

        $this->expectException(RequestExceptionInterface::class);
        $this->expectExceptionMessage('Empty protocol version');

        $this->requestValidator->validate($request);
    }

    public function testEmptyUri(): void
    {
        $request = $this->createMock(RequestInterface::class);

        $request->method('getMethod')
            ->willReturn('GET');

        $request->method('getProtocolVersion')
            ->willReturn('1.1');

        $uri = $this->createMock(UriInterface::class);

        $uri->method('__toString')
            ->willReturn('');

        $request->method('getUri')
            ->willReturn($uri);

        $this->expectException(RequestExceptionInterface::class);
        $this->expectExceptionMessage('Empty URI');

        $this->requestValidator->validate($request);
    }
}
