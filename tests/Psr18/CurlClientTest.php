<?php

namespace Rauc\Psr18;

use PHPUnit\Framework\TestCase;
use Psr\Http\Client\NetworkExceptionInterface;
use Psr\Http\Client\RequestExceptionInterface;
use Rauc\Psr17\Factory;
use Rauc\Psr7\{RequestValidatorInterface};
use Rauc\CurlWrapperInterface;

class CurlClientTest extends TestCase
{
    private Factory $psr17Factory;
    private CurlWrapperInterface $curlWrapper;
    private RequestValidatorInterface $psr7RequestValidator;
    private CurlClient $curlClient;

    public function setUp(): void
    {
        $this->psr17Factory = new Factory();
        $this->curlWrapper = $this->createMock(CurlWrapperInterface::class);
        $this->psr7RequestValidator = $this->createMock(RequestValidatorInterface::class);
        $this->curlClient = new CurlClient($this->curlWrapper, $this->psr17Factory, $this->psr7RequestValidator);
    }

    public function testInit(): void
    {
        $request = $this->createRequest('GET', 'https://example.com/posts/1');

        $this->curlWrapper->expects($this->once())
            ->method('init')
            ->willReturn($this->curlWrapper);

        $this->curlClient->sendRequest($request);
    }

    public function testSettingMethod(): void
    {
        $request = $this->createRequest('GET', 'https://example.com/posts/1');

        $this->curlWrapper->method('init')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->expects($this->once())
            ->method('setHttpMethod')
            ->with('GET')
            ->willReturn($this->curlWrapper);

        $this->curlClient->sendRequest($request);
    }

    public function testSettingUrl(): void
    {
        $request = $this->createRequest('GET', 'https://example.com/posts/1');

        $this->curlWrapper->method('init')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setHttpMethod')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->expects($this->once())
            ->method('setUrl')
            ->with('https://example.com/posts/1')
            ->willReturn($this->curlWrapper);

        $this->curlClient->sendRequest($request);
    }

    public function testSettingTimeout(): void
    {
        $request = $this->createRequest('GET', 'https://example.com/posts/1');

        $this->curlWrapper->method('init')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setHttpMethod')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setUrl')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->expects($this->once())
            ->method('setTimeout')
            ->with($this->isType('int'))
            ->willReturn($this->curlWrapper);

        $this->curlClient->sendRequest($request);
    }

    public function testNotSettingPostFieldsOnGetDeleteOptionsHeadConnectAndTraceMethods(): void
    {
        $body = 'username=foo&password=bar';
        $getRequest = $this->createRequest('GET', 'https://example.com/posts/1', $body);
        $deleteRequest = $this->createRequest('DELETE', 'https://example.com/posts/1', $body);
        $optionsRequest = $this->createRequest('OPTIONS', 'https://example.com/posts/1', $body);
        $headRequest = $this->createRequest('HEAD', 'https://example.com/posts/1', $body);
        $connectRequest = $this->createRequest('CONNECT', 'https://example.com/posts/1', $body);
        $traceRequest = $this->createRequest('TRACE', 'https://example.com/posts/1', $body);

        $this->curlWrapper->method('init')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setHttpMethod')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setUrl')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setTimeout')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->expects($this->never())
            ->method('setPostFields');

        $this->curlClient->sendRequest($getRequest);
        $this->curlClient->sendRequest($deleteRequest);
        $this->curlClient->sendRequest($optionsRequest);
        $this->curlClient->sendRequest($headRequest);
        $this->curlClient->sendRequest($connectRequest);
        $this->curlClient->sendRequest($traceRequest);
    }

    public function testSettingPostFieldsOnPostPutAndPatchMethods(): void
    {
        $body = 'username=foo&password=bar';
        $postRequest = $this->createRequest('POST', 'https://example.com/posts', $body);
        $putRequest = $this->createRequest('PUT', 'https://example.com/posts/1', $body);
        $patchRequest = $this->createRequest('PATCH', 'https://example.com/posts/1', $body);

        $this->curlWrapper->method('init')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setHttpMethod')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setUrl')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setTimeout')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->expects($this->exactly(3))
            ->method('setPostFields')
            ->with('username=foo&password=bar')
            ->willReturn($this->curlWrapper);

        $this->curlClient->sendRequest($postRequest);
        $this->curlClient->sendRequest($putRequest);
        $this->curlClient->sendRequest($patchRequest);
    }

    public function testNotSettingPostFieldsOnPostPutAndPatchMethodsWhenBodyIsEmpty(): void
    {
        $postRequest = $this->createRequest('POST', 'https://example.com/posts');
        $putRequest = $this->createRequest('PUT', 'https://example.com/posts/1');
        $patchRequest = $this->createRequest('PATCH', 'https://example.com/posts/1');

        $this->curlWrapper->method('init')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setHttpMethod')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setUrl')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setTimeout')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->expects($this->never())
            ->method('setPostFields');

        $this->curlClient->sendRequest($postRequest);
        $this->curlClient->sendRequest($putRequest);
        $this->curlClient->sendRequest($patchRequest);
    }

    public function testSettingRequestHeaders(): void
    {
        $body = 'username=foo&password=bar';

        $headers = [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Host' => 'example.com',
        ];

        $request = $this->createRequest('POST', 'https://example.com/posts', $body, $headers);

        $this->curlWrapper->method('init')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setHttpMethod')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setUrl')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setTimeout')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setPostFields')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->expects($this->once())
            ->method('setRequestHeaders')
            ->with($headers)
            ->willReturn($this->curlWrapper);

        $this->curlClient->sendRequest($request);
    }

    public function testExecutingRequest(): void
    {
        $request = $this->createRequest('GET', 'https://example.com/posts/1');

        $this->curlWrapper->method('init')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setHttpMethod')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setUrl')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setTimeout')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->expects($this->once())
            ->method('execute')
            ->willReturn($this->curlWrapper);

        $this->curlClient->sendRequest($request);
    }

    public function testClosingCurlSession(): void
    {
        $request = $this->createRequest('GET', 'https://example.com/posts/1');

        $this->curlWrapper->method('init')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setHttpMethod')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setUrl')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setTimeout')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('execute')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->expects($this->once())
            ->method('close');

        $this->curlClient->sendRequest($request);
    }

    public function testSuccessfulResponse(): void
    {
        $body = json_encode([
            'title' => 'Post Title',
            'author' => 'Post Author',
        ]);

        $headers = [
            'Content-Type' => 'application/json',
            'Host' => 'example.com'
        ];

        $request = $this->createRequest('POST', 'https://example.com/posts', $body, $headers);

        $this->curlWrapper->method('init')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setHttpMethod')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setUrl')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setTimeout')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setPostFields')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setRequestHeaders')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('execute')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('getErrorCode')
            ->willReturn(0);

        $this->curlWrapper->method('getErrorMessage')
            ->willReturn('');

        $this->curlWrapper->method('getStatusCode')
            ->willReturn(201);

        $this->curlWrapper->method('getResponseHeaders')
            ->willReturn([
                'Content-Type' => 'application/json',
            ]);

        $this->curlWrapper->method('getResponse')
            ->willReturn($body);

        $response = $this->curlClient->sendRequest($request);
        $responseBody = $response->getBody()
            ->__toString();

        $this->assertSame(201, $response->getStatusCode());
        $this->assertSame($body, $responseBody);
        $this->assertSame([
            'Content-Type' => ['application/json'],
        ], $response->getHeaders());
    }

    public function testCurlError(): void
    {
        $request = $this->createRequest('GET', 'https://example.com/posts/1');

        $this->curlWrapper->method('init')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setHttpMethod')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setUrl')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setTimeout')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setPostFields')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('setRequestHeaders')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('execute')
            ->willReturn($this->curlWrapper);

        $this->curlWrapper->method('getErrorCode')
            ->willReturn(CURLE_OPERATION_TIMEDOUT);

        $this->curlWrapper->method('getErrorMessage')
            ->willReturn('Error message');

        $this->expectException(NetworkExceptionInterface::class);
        $this->expectExceptionMessage('Error message');

        $this->curlClient->sendRequest($request);
    }

    public function testInvalidRequest(): void
    {
        $request = $this->createRequest('GET', 'https://example.com/posts/1');

        $this->psr7RequestValidator->method('validate')
            ->willThrowException(new RequestException('Error message', $request));

        $this->expectException(RequestExceptionInterface::class);
        $this->expectExceptionMessage('Error message');

        $this->curlClient->sendRequest($request);
    }

    private function createRequest(string $method, string $url, string $body = '', array $headers = [])
    {
        $request = $this->psr17Factory->createRequest($method, $url);

        if ($body !== '') {
            $stream = $this->psr17Factory->createStream($body);
            $request = $request->withBody($stream);
        }

        if (!empty($headers)) {
            foreach ($headers as $header => $value) {
                $request = $request->withHeader($header, $value);
            }
        }

        return $request;
    }
}
