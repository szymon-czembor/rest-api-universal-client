# REST API Universal Client

Usage example can be found here: https://bitbucket.org/szymon-czembor/rauc-example/

This library provides few classes for consuming REST APIs. The main class is *ApiClient* which has several methods for sending HTTP requests and fetching responses.

### Creating API client instance
*ApiClient* has few dependencies. To simplify process of creating instance of this class, the library provides a factory class with static method.

```
$apiClient = \Rauc\ApiClientFactory::create();
```

### Setting API base URL

To consume REST API set its base URL:

```
$apiClient->setBaseUrl('https://example.com');
```

### Sending requests

API client supports the following HTTP methods: GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD. Each verb has a corresponding method in *ApiClient* class. For example to send GET request use *get* method:

```
$response = $apiClient->get('/posts/1');
```

### Responses

Request methods of *ApiClient* class return responses compliant with PSR-7.

### Exceptions

If something goes wrong methods can throw *ApiClientException*.

### Authentication

Library provides 2 methods of authentication:
- Basic Authentication
- JWT

Example of accessing resource which needs Basic Authentication:

```
$basicAuthentication = new \Rauc\Authentication\BasicAuthentication();

$basicAuthentication->setUsername('username')
    ->setPassword('password');

$apiClient->setAuthMethod($basic);

$response = $apiClient->get('/posts/1');
```